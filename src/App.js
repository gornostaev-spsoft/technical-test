import React, { useEffect, useState } from 'react';

const colorInterp = (left, right, percentage) => {
  const newColor = {};
  const channel = ["r", "g", "b"];

  for (var i = 0; i < channel.length; i++) {
      const c = channel[i];
      newColor[c] = Math.round(left[c] + (right[c] - left[c]) * percentage / 100);
  }
  return newColor;
};

const App = () => {
  const [isFirstScreen, setIsFirstScreen] = useState(true);
  const [isLoading, setLoading] = useState(false);

  const [weather, setWeather] = useState(null);
  const [background, setBackground] = useState('#fff');

  const allowToDetect = () => setIsFirstScreen(false);

  const calculateBackgroundColor = (temp) => {
    if (temp <= -10) setBackground('#00ffff');
    if (temp === 10) setBackground('#fff700');
    if (temp >= 30) setBackground('#ff8c00');

    if (temp > -10 && temp < 10) {
      const percentage = (-10 - temp) / 20 * 100;
      const color = colorInterp(
        {r: 0, g: 255, b: 255},
        {r: 255, g: 247, b: 0},
        Math.abs(percentage),
      );
      setBackground(`rgb(${color.r}, ${color.g}, ${color.b})`);
    }
    if (temp > 10 && temp < 30) {
      const percentage = (10 - temp) / 20 * 100;
      const color = colorInterp(
        {r: 255, g: 247, b: 0},
        {r: 255, g: 140, b: 0},
        Math.abs(percentage),
      );
      setBackground(`rgb(${color.r}, ${color.g}, ${color.b})`);
    }
  }

  useEffect(() => {
    if (isFirstScreen) return;

    const fetchData = async (position) => {
      setLoading(true);

      if (
        !position ||
        !position.coords ||
        !position.coords.latitude ||
        !position.coords.longitude
      ) return null;
      const {
        latitude,
        longitude,
      } = position.coords;

      const url = `https://fcc-weather-api.glitch.me/api/current?lon=${longitude}&lat=${latitude}`;
      const response = await fetch(url);
      const result = await response.json();

      if (
        result &&
        result.weather &&
        result.weather[0] &&
        result.main
      ) {
        setWeather({
          ...result.weather[0],
          ...result.main,
        });
        calculateBackgroundColor(result.main.temp);
        setLoading(false);
      }
    }

    if (
      navigator &&
      navigator.geolocation &&
      navigator.geolocation.getCurrentPosition
    ) {
      navigator.geolocation.getCurrentPosition(fetchData, () => {});
    }
  }, [isFirstScreen]);

  return (
    <main
      id="main"
      className="main"
      role="main"
      style={{
        background,
      }}
    >
      {isFirstScreen && (
        <div className="main--first-screen">
          <div className="main--first-screen-title">
            Do you allow to detect your location?
          </div>
          <button onClick={allowToDetect}>Yes</button>
        </div>
      )}

      {isLoading && (
        <div className="main--loading">
          Loading...
        </div>
      )}

      {(!isLoading && weather) && (
        <div className="main--weather-icon">
          <img
            src={weather.icon}
            alt={weather.description}
          />
        </div>
      )}
    </main>
  );
};

export default App;
